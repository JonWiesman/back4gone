class Input
{
    constructor(canvas)
    {
        this.canvas = canvas;
        this.x = 0;
        this.y = 0;
        this.dx = 0;
        this.dy = 0;
        this.isTouchActive = false;
        this.isNewTouch = false;
        this.wheel = 0.0;
        this.keys = {};
        this.keysJustPressed = {};
        this.pressedKeysThisFrame = [];


        canvas.addEventListener("mousedown", e => { this.isTouchActive = true; this.isNewTouch = true; }, true);
        canvas.addEventListener("mouseup", e => { this.isTouchActive = false }, true);
        canvas.addEventListener("mousemove", e => { this.SetInputPos(e); e.preventDefault(); }, true );
        canvas.addEventListener("wheel", e => { this.wheel = e.deltaY; e.preventDefault(); }, true );
        canvas.addEventListener("touchstart", e => { this.SetInputPos(e.touches[0]); this.isTouchActive = true; this.isNewTouch = true; this.dx = 0; this.dy = 0; e.preventDefault(); }, true );
        canvas.addEventListener("touchend", e => { this.isTouchActive = false; e.preventDefault(); }, true );
        canvas.addEventListener("touchcancel", e => { this.isTouchActive = false; e.preventDefault(); }, true );
        canvas.addEventListener("touchmove", e => { this.SetInputPos(e.touches[0]); e.preventDefault(); }, true );

        window.addEventListener("keydown", e => this.setKeyState(e, true));
        window.addEventListener("keyup", e => this.setKeyState(e, false));
        
    }

    SetInputPos(event)
    {
        let xLast = this.x;
        let yLast = this.y;

        this.x = event.pageX - this.canvas.offsetLeft;
        this.y = event.pageY - this.canvas.offsetTop;
        this.dx = this.x - xLast;
        this.dy = this.y - yLast;
        this.canvasx = Math.floor(this.x / 2);
        this.canvasy = Math.floor(this.y / 2);
    }

    setKeyState(event, isOn)
    {
        let keyName = event.key.toLowerCase();
        this.keysJustPressed[keyName] = isOn && (this.keys[keyName] === false || this.keys[keyName] === undefined);
        this.keys[keyName] = isOn;
        
        if(isOn)
        {
            this.pressedKeysThisFrame.push(keyName);
        }

        // Hack: prevent arrow keys from scrolling the page
        if (keyName === "arrowup" || keyName === "arrowdown" || keyName === "arrowleft" || keyName === "arrowright")
        {
            event.preventDefault();
        }
    }

    PostUpdate()
    {
        this.isNewTouch = false;
        this.dx = 0;
        this.dy = 0;
        this.wheel = 0.0;

        Object.keys(this.keysJustPressed).forEach(key => this.keysJustPressed[key] = false);

    }
}