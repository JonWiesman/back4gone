let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d", { alpha: false });
let input = new Input(canvas);
let lastTime = 0;
let mgl = new MGL(canvas, ctx, canvas.width / canvas.height, 0xFFFFC9BF);

let polyInfo = '';

let game = new Game();

function GameLoop(curTime)
{
    let dt = Math.min((curTime - lastTime) / 1000.0, 0.2);	
    lastTime = curTime;

    game.Update(dt);

    mgl.ClearBuffers();
    game.Render();
    mgl.RenderBuffers();

    //TEMP!	
    ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
    ctx.fillRect(0, 0, 400, 16);
    ctx.font = `10px Arial`;	
    ctx.fillStyle = "#FFF";	
    let fps = 1.0 / dt;
    ctx.fillText(`${Math.floor(fps)}. ${polyInfo}`, 2, 10);

    input.PostUpdate();
    window.requestAnimationFrame(GameLoop);
}

window.requestAnimationFrame(GameLoop);