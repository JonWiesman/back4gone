// game.js

let grassHeight = 0;
let dirtHeight = 0.0001;
let chalkHeight = 0.0002;
let shadowHeight = 0.0003;

let pitchStart = new Vec3(42.77 * foot, 3, 42.77 * foot);

class Game
{
    constructor()
    {
        this.scene = new Scene(mgl, input);
        this.input = input;
        this.mgl = mgl;
        this.Reset();

        this.selectedObject = null;
    }

    Reset()
    {
        let grassPlane = {minx: -1, minz: -1, maxx: 205, maxz: 205}

        this.age = 0;
        this.pitchDelay = 1;
        this.objects = [];
        this.stadium = new Stadium();

        this.ball = new Ball(pitchStart);

        this.AddObject(this.stadium);
        this.AddObject(this.ball);

        this.mgl.lights.push(new Light(new Vec3(-1.0, 0.0, 0.0), new Color(0.3, 0.3, 0.3), new Color(1.0, 1.0, 1.0)));
        this.mgl.lights.push(new Light(new Vec3(0.0, -1.0, 0.0), new Color(0.3, 0.3, 0.3), new Color(1.0, 1.0, 1.0)));

        this.mgl.SetCameraLookAt(new Vec3(-3.0, 2.0, -3.0),
                                 new Vec3(0.0, 1.0, 0.0),
                                 new Vec3(0, 1, 0));
    }

    AddObject(o)
    {
        this.objects.push(o);
    }

    Update(dt)
    {
        this.age += dt;
        if(this.input.keysJustPressed['r'])
        {
            this.Reset();
        }
        this.objects.forEach(obj => obj.Update(dt));

        this.pitchDelay -= dt;
        if(this.pitchDelay < 0 && !this.ball.pitching)
        {
            this.ball.Pitch();
            this.pitchDelay = 1;
        }
        

    }

    Render(dt)
    {
        this.objects.forEach(obj => obj.Render(this.mgl));
    }

    SetSelectedObject(object)
    {
        this.selectedObject = object;
    }
}