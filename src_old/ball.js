class Ball extends mglObj
{
    constructor(pos)
    {
        super();
        this.radius = 0.038;
        this.ball = new Icosphere(pos, this.radius, 3, null, new Color(1, 1, 1));
        this.shadow = new Icosphere(new Vec3(pos.x, shadowHeight, pos.z), this.radius, 3, null, new Color(0, 0, 0));
        this.SetPosition(pos);

        this.age = 0;
        this.delta = new Vec3(0, 0, 0);
        this.pitching = false;
        this.delta 
    }

    SetPosition(pos)
    {
        this.pos = pos.Copy();
        this.ball.tm = new Matrix4x4(new Vec3(this.radius, 0, 0),
                                     new Vec3(0, this.radius, 0),
                                     new Vec3(0, 0, this.radius),
                                     pos);

        this.shadow.tm = new Matrix4x4(new Vec3(this.radius, 0, 0),
                                       new Vec3(0, this.radius, 0),
                                       new Vec3(0, 0, this.radius),
                                       new Vec3(pos.x, shadowHeight, pos.z));
    }

    Render(mgl)
    {
        mgl.RenderObject(this.ball);
        mgl.RenderObject(this.shadow);
    }

    Update(dt)
    {
        this.age += dt;

        this.delta.y -= dt * 9.8;
        let newPos = this.pos.Add(this.delta.Scale(dt));
        if(newPos.y < this.radius / 2)
        {
            newPos.y = this.radius / 2;
            this.delta = new Vec3(0, 0, 0);
            this.pitching = false;
        }
        
        this.SetPosition(newPos);

        if(newPos.x < 0 || newPos.z < 0)
        {
            this.pitching = false;
        }


    }

    Pitch()
    {
        if(this.pitching)
        {
            return;
        }
        this.SetPosition(pitchStart);
        let target = new Vec3(-0.5 * Math.random() * 1, 1.5 + Math.random() * 1.5, -0.5 * Math.random() * 1);
        let v = (65 + Math.random() * 10) * 0.447;
        this.delta = target.Sub(pitchStart).Normalize().Scale(v);

        this.pitching = true;
    }

}