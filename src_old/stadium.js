

let dirtCoords = [
    -1, 1,

];

class Stadium extends mglObj
{
    constructor()
    {
        super();
        this.mound = new Icosphere(new Vec3(42.77 * foot, -1.5 * foot, 42.77 * foot), 1, 3, null, new Color(0.5, 0.2, 0));

        this.grass = new mglObj();
        let green = new Color(0, 1, 0);
        let blue = new Color(0, 0, 0);
        for(let x = -15; x < 0; x += 5)
        {
            for(let z = -15; z < 0; z+= 5)
            {
                let i0 = this.grass.verts.length;
                this.grass.verts.push(new Vec3(x, 0, z));
                this.grass.verts.push(new Vec3(x, 0, z + 5));
                this.grass.verts.push(new Vec3(x + 5, 0, z + 5));
                this.grass.verts.push(new Vec3(x + 5, 0, z));
                for(let i = 0; i < 4; i++)
                {
                    this.grass.colors.push(green);
                    this.grass.uvs.push(new Vec3(0, 0, 0));
                }
                this.grass.indices.push(i0);
                this.grass.indices.push(i0 + 1);
                this.grass.indices.push(i0 + 2);
                // this.grass.indices.push(i0);
                // this.grass.indices.push(i0 + 2);
                // this.grass.indices.push(i0 + 3);
            }

        }
        this.grass.texture = null;

    }


    Render(mgl)
    {
        mgl.RenderObject(this.mound);
        mgl.RenderObject(this.grass);
    }
}