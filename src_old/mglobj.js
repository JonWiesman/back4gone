class mglObj 
{
    constructor()
    {
        this.verts = [];
        this.indices = [];
        this.colors = [];
        this.uvs = [];
        this.tm = new Matrix4x4(new Vec3(1, 0, 0),
                                new Vec3(0, 1, 0),
                                new Vec3(0, 0, 1),
                                new Vec3(0, 0, 0));
    }

    Update(dt)
    {

    }

    Render(mgl)
    {
        mgl.RenderObject(this);
    }
}