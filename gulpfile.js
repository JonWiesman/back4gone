var gulp = require("gulp");
var concat = require("gulp-concat");
var htmlreplace = require("gulp-html-replace");
var closureCompiler = require("google-closure-compiler").gulp();
const zip = require("gulp-zip");

var assetFiles = 
[
    "style.css",
];

var sourceFiles =
[
    // <script src="src/aw.js"></script>
    // <script src="src/TinyMusic.js"></script>
    // <script src="src/vec3.js"></script>
    // <script src="src/brushes.js"></script>
    // <script src="src/matrix4x4.js"></script>
    // <script src="src/entityBase.js"></script>
    // <script src="src/stateBase.js"></script>
    // <script src="src/stateGame.js"></script>
    // <script src="src/stateMenu.js"></script>
    // <script src="src/stateSplash.js"></script>
    // <script src="src/ball.js"></script>
    // <script src="src/stadium.js"></script>
    // <script src="src/main.js"></script>


    // aw
    "src/aw.js",
    "src/TinyMusic.js",
    "src/vec3.js",
    "src/brushes.js",
    "src/matrix4x4.js",
    "src/entityBase.js",
    "src/stateBase.js",
    "src/consts.js",
    "src/stateGame.js",
    "src/stateMenu.js",
    "src/stateSplash.js",
    "src/ball.js",
    "src/stadium.js",
    // Game logic
    "./src/main.js",
];

var outputFiles =
[
    "./build/index.html",
    "./build/style.css",
    "./build/concat.min.js",
];

gulp.task("build", ["zip"], () =>
{
});

gulp.task("zip", ["minify_js", "copy_assets"], () =>
    gulp.src(outputFiles)
        .pipe(zip("back4gone.zip"))
        .pipe(gulp.dest("./build/"))
);

gulp.task("minify_js", ["build_js", "build_html"], () =>
{
    return gulp.src("./build/concat.js")
        .pipe(closureCompiler(
            {
                compilation_level: "ADVANCED",
                warning_level: "QUIET",
                language_in: "ECMASCRIPT6_STRICT",
                language_out: "ECMASCRIPT5_STRICT",
                /*output_wrapper: "(function(){\n%output%\n}).call(this)",*/
                js_output_file: "concat.min.js"
            }))
        .pipe(gulp.dest("./build/"));
});

gulp.task("copy_assets", () =>
{
    return gulp.src(assetFiles).pipe(gulp.dest("./build/"));
});

gulp.task("build_js", () =>
{
	return gulp.src(sourceFiles)
            .pipe(concat("concat.js"))
            .pipe(gulp.dest("./build/"));
});

gulp.task("build_html", () =>
{
    gulp.src("index.html")
        .pipe(htmlreplace({ "js": "concat.min.js" }))
        .pipe(gulp.dest("./build/"));
});